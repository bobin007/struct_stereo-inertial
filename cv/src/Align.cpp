#include "ygz/Settings.h"
#include "ygz/Align.h"
#include "xmmintrin.h"

namespace ygz {


    bool Align2D_SSE(
            const cv::Mat &cur_img,
            uint8_t *ref_patch_with_border,
            uint8_t *ref_patch,
            const int n_iter,
            Vector2f &cur_px_estimate) {

        bool converged = false;

        // compute derivative of template and prepare inverse compositional
        float __attribute__ (( __aligned__ ( 16 ))) ref_patch_dx[align_patch_area];
        float __attribute__ (( __aligned__ ( 16 ))) ref_patch_dy[align_patch_area];
        Matrix3f H;
        float m[9];
        Eigen::Map<Matrix3f> Hm(m);
        Hm.setZero();
        H.setZero();
        float Hm9[9][4];
        // compute gradient and hessian
        const int ref_step = align_patch_size + 2;
        float *it_dx = ref_patch_dx;
        float *it_dy = ref_patch_dy;
        for (int y = 0; y < align_patch_size;) {
            uint8_t *it = ref_patch_with_border + (y + 1) * ref_step + 1;
            __m128i f1 = _mm_loadu_si128((__m128i *) it + 1);//_mm_set1_ps(it[1]);
            __m128i f2 = _mm_loadu_si128((__m128i *) it - 1);
            __m128i const zero = _mm_setzero_si128();
            f1 = _mm_sub_epi8(f1, f2);
            f2 = _mm_unpacklo_epi8(f1, zero); // low
            f1 = _mm_unpackhi_epi8(f1, zero); // high
            f1 = _mm_srai_epi16(f1, 1);
            f2 = _mm_srai_epi16(f2, 1);

            __m128i f3 = _mm_loadu_si128((__m128i *) (it + ref_step));
            __m128i f4 = _mm_loadu_si128((__m128i *) (it - ref_step));
            f3 = _mm_sub_epi8(f3, f4);
            f4 = _mm_unpacklo_epi8(f3, zero);
            f3 = _mm_unpackhi_epi8(f3, zero);
            f3 = _mm_srai_epi16(f3, 1);
            f4 = _mm_srai_epi16(f4, 1);

            __m128i J0 = _mm_unpacklo_epi16(f2, zero);
            __m128 J0f = _mm_cvtepi32_ps(J0);
            _mm_store_ps(it_dx, J0f);
            __m128i J1 = _mm_unpacklo_epi16(f4, zero);
            __m128 J1f = _mm_cvtepi32_ps(J1);
            _mm_store_ps(it_dy, J1f);

            _mm_store_ps(Hm9[0], _mm_mul_ps(J0f, J0f));
            _mm_store_ps(Hm9[1], _mm_mul_ps(J0f, J1f));
            _mm_store_ps(Hm9[2], J0f);
            _mm_store_ps(Hm9[3], _mm_mul_ps(J1f, J0f));
            _mm_store_ps(Hm9[4], _mm_mul_ps(J1f, J1f));
            _mm_store_ps(Hm9[5], J1f);
            _mm_store_ps(Hm9[6], J0f);
            _mm_store_ps(Hm9[7], J1f);


            for (int i = 0; i < 8; ++i) {
                for (int j = 0; j < 4; ++j) {
                    m[i] += Hm9[i][j];
                }
            }
            m[8] = 4;
            H += Hm;

            J0 = _mm_unpackhi_epi16(f2, zero);
            J0f = _mm_cvtepi32_ps(J0);
            _mm_store_ps(it_dx + 4, J0f);
            J1 = _mm_unpackhi_epi16(f4, zero);
            J1f = _mm_cvtepi32_ps(J1);
            _mm_store_ps(it_dy + 4, J1f);

            _mm_store_ps(Hm9[0], _mm_mul_ps(J0f, J0f));
            _mm_store_ps(Hm9[1], _mm_mul_ps(J0f, J1f));
            _mm_store_ps(Hm9[2], J0f);
            _mm_store_ps(Hm9[3], _mm_mul_ps(J1f, J0f));
            _mm_store_ps(Hm9[4], _mm_mul_ps(J1f, J1f));
            _mm_store_ps(Hm9[5], J1f);
            _mm_store_ps(Hm9[6], J0f);
            _mm_store_ps(Hm9[7], J1f);


            for (int i = 0; i < 9; ++i) {
                for (int j = 0; j < 4; ++j) {
                    m[i] += Hm9[i][j];
                }
            }
            m[8] = 4;
            H += Hm;

            J0 = _mm_unpacklo_epi16(f1, zero);
            J0f = _mm_cvtepi32_ps(J0);
            _mm_store_ps(it_dx + 8, J0f);
            J1 = _mm_unpacklo_epi16(f3, zero);
            J1f = _mm_cvtepi32_ps(J1);
            _mm_store_ps(it_dy + 8, J1f);

            _mm_store_ps(Hm9[0], _mm_mul_ps(J0f, J0f));
            _mm_store_ps(Hm9[1], _mm_mul_ps(J0f, J1f));
            _mm_store_ps(Hm9[2], J0f);
            _mm_store_ps(Hm9[3], _mm_mul_ps(J1f, J0f));
            _mm_store_ps(Hm9[4], _mm_mul_ps(J1f, J1f));
            _mm_store_ps(Hm9[5], J1f);
            _mm_store_ps(Hm9[6], J0f);
            _mm_store_ps(Hm9[7], J1f);


            for (int i = 0; i < 9; ++i) {
                for (int j = 0; j < 4; ++j) {
                    m[i] += Hm9[i][j];
                }
            }
            m[8] = 4;
            H += Hm;

            J0 = _mm_unpackhi_epi16(f1, zero);
            J0f = _mm_cvtepi32_ps(J0);
            _mm_store_ps(it_dx + 12, J0f);

            J1 = _mm_unpackhi_epi16(f3, zero);
            J1f = _mm_cvtepi32_ps(J1);
            _mm_store_ps(it_dy + 12, J1f);
            _mm_store_ps(Hm9[0], _mm_mul_ps(J0f, J0f));
            _mm_store_ps(Hm9[1], _mm_mul_ps(J0f, J1f));
            _mm_store_ps(Hm9[2], J0f);
            _mm_store_ps(Hm9[3], _mm_mul_ps(J1f, J0f));
            _mm_store_ps(Hm9[4], _mm_mul_ps(J1f, J1f));
            _mm_store_ps(Hm9[5], J1f);
            _mm_store_ps(Hm9[6], J0f);
            _mm_store_ps(Hm9[7], J1f);


            for (int i = 0; i < 9; ++i) {
                for (int j = 0; j < 4; ++j) {
                    m[i] += Hm9[i][j];
                }
            }
            m[8] = 4;
            H += Hm;

            y += 128 / 8 / align_patch_size;
//            it += 128 / 8;
            it_dx += 128 / 8;
            it_dy += 128 / 8;

//            for (int x = 0; x < align_patch_size; ++x, ++it, ++it_dx, ++it_dy) {
//                Vector3f J;
//
//                J[0] = 0.5 * (it[1] - it[-1]);
//                J[1] = 0.5 * (it[ref_step] - it[-ref_step]);
//                J[2] = 1;
//                *it_dx = J[0];
//                *it_dy = J[1];
//                H += J * J.transpose();
//            }
        }
        Matrix3f Hinv = H.inverse();

        // H is singular, maybe caused by over explosure or completely dark parts
        if (isnan(Hinv(0, 0))) {
            cur_px_estimate << -1, -1;
            return false;
        }

        float mean_diff = 0;

        // Compute pixel location in new image:
        float u = cur_px_estimate.x();
        float v = cur_px_estimate.y();

        // termination condition
        const float min_update_squared = 0.03 * 0.03;
        const int cur_step = cur_img.step.p[0];
        Vector3f update;
        update.setZero();
        float chi2 = 0;
        for (int iter = 0; iter < n_iter; ++iter) {
            chi2 = 0;
            int u_r = floor(u);
            int v_r = floor(v);
            if (u_r < align_halfpatch_size || v_r < align_halfpatch_size ||
                u_r >= cur_img.cols - align_halfpatch_size ||
                v_r >= cur_img.rows - align_halfpatch_size)
                break;

            // compute interpolation weights
            float subpix_x = u - u_r;
            float subpix_y = v - v_r;
            float wTL = (1.0 - subpix_x) * (1.0 - subpix_y);
            float wTR = subpix_x * (1.0 - subpix_y);
            float wBL = (1.0 - subpix_x) * subpix_y;
            float wBR = subpix_x * subpix_y;

            // loop through search_patch, interpolate
            uint8_t *it_ref = ref_patch;
            float *it_ref_dx = ref_patch_dx;
            float *it_ref_dy = ref_patch_dy;
            Vector3f Jres;
            float Jres_arr[3][4];
            float res2[4];
            float jres[3];
            Eigen::Map<Vector3f> Jresm(jres);
            Jresm.setZero();
            Jres.setZero();
            for (int y = 0; y < align_patch_size; ++y) {
                uint8_t *it = (uint8_t *) cur_img.data + (v_r + y - align_halfpatch_size) * cur_step + u_r -
                              align_halfpatch_size;
                for (int x = 0; x < align_patch_size; ) {
                    __m128i f1 = _mm_loadu_si128((__m128i *) it);
                    f1 = _mm_cvtepi8_epi16(f1);
                    f1 = _mm_cvtepi16_epi32(f1);
                    __m128 f2 = _mm_cvtepi32_ps(f1);
                    __m128 f3 = _mm_set_ps(wTL, wTL, wTL, wTL);
                    __m128 sum = _mm_mul_ps(f2, f3);

                    f1 = _mm_loadu_si128((__m128i *) it + 1);
                    f1 = _mm_cvtepi8_epi16(f1);
                    f1 = _mm_cvtepi16_epi32(f1);
                    f2 = _mm_cvtepi32_ps(f1);
                    f3 = _mm_set_ps(wTR, wTR, wTR, wTR);
                    sum = _mm_add_ps(sum, _mm_mul_ps(f3, f2));

                    f1 = _mm_loadu_si128((__m128i *) it + cur_step);
                    f1 = _mm_cvtepi8_epi16(f1);
                    f1 = _mm_cvtepi16_epi32(f1);
                    f2 = _mm_cvtepi32_ps(f1);
                    f3 = _mm_set_ps(wBL, wBL, wBL, wBL);
                    sum = _mm_add_ps(sum, _mm_mul_ps(f3, f2));

                    f1 = _mm_loadu_si128((__m128i *) it + cur_step + 1);
                    f1 = _mm_cvtepi8_epi16(f1);
                    f1 = _mm_cvtepi16_epi32(f1);
                    f2 = _mm_cvtepi32_ps(f1);
                    f3 = _mm_set_ps(wBR, wBR, wBR, wBR);
                    sum = _mm_add_ps(sum, _mm_mul_ps(f3, f2));

                    f1 = _mm_loadu_si128((__m128i *) it_ref);
                    f1 = _mm_cvtepi8_epi16(f1);
                    f1 = _mm_cvtepi16_epi32(f1);
                    f2 = _mm_cvtepi32_ps(f1);
                    f3 = _mm_set_ps(mean_diff, mean_diff, mean_diff, mean_diff);
                    sum = _mm_sub_ps(sum, f2);
                    __m128 res = _mm_add_ps(sum, f3);

                    f2 = _mm_load_ps(it_ref_dx);
                    __m128 const zero = _mm_setzero_ps();
                    sum = _mm_mul_ps(res, f2);
                    sum = _mm_sub_ps(zero, sum);
                    _mm_store_ps(Jres_arr[0], sum);
                    for (int i = 0; i < 4; ++i) {
                        m[0] += Jres_arr[0][i];
                    }

                    f2 = _mm_load_ps(it_ref_dy);
                    sum = _mm_mul_ps(res, f2);
                    sum = _mm_sub_ps(zero, sum);
                    _mm_store_ps(Jres_arr[0], sum);
                    for (int i = 0; i < 4; ++i) {
                        m[1] += Jres_arr[0][i];
                    }

                    sum = _mm_sub_ps(zero, res);
                    _mm_store_ps(Jres_arr[0], sum);
                    for (int i = 0; i < 4; ++i) {
                        m[2] += Jres_arr[0][i];
                    }
                    res = _mm_mul_ps(res, res);
                    _mm_store_ps(res2, res);
                    for (int j = 0; j < 4; ++j) {
                        chi2 += res2[j];
                    }

                    x += 4;
                    it += 4;
                    it_ref += 4;
                    it_ref_dx += 4;
                    it_ref_dy += 4;


//                    float search_pixel = wTL * it[0] + wTR * it[1] + wBL * it[cur_step] + wBR * it[cur_step + 1];
//                    float res = search_pixel - *it_ref + mean_diff;
//                    Jres[0] -= res * (*it_ref_dx);
//                    Jres[1] -= res * (*it_ref_dy);
//                    Jres[2] -= res;
//                    chi2 += res * res;
                }
            }
            update = Hinv * Jres;
            u += update[0];
            v += update[1];

            mean_diff += update[2];
            if (update[0] * update[0] + update[1] * update[1] < min_update_squared) {
                converged = true;
                break;
            }
        }

        if (isnan(u) || isnan(v)) {
            cur_px_estimate << -1, -1;
            return false;
        }
        cur_px_estimate << u, v;
        return converged;
    }

    bool Align2D(
            const cv::Mat &cur_img,
            uint8_t *ref_patch_with_border,
            uint8_t *ref_patch,
            const int n_iter,
            Vector2f &cur_px_estimate) {

        bool converged = false;

        // compute derivative of template and prepare inverse compositional
        float __attribute__ (( __aligned__ ( 16 ))) ref_patch_dx[align_patch_area];
        float __attribute__ (( __aligned__ ( 16 ))) ref_patch_dy[align_patch_area];
        Matrix3f H;
        H.setZero();

        // compute gradient and hessian
        const int ref_step = align_patch_size + 2;
        float *it_dx = ref_patch_dx;
        float *it_dy = ref_patch_dy;
        for (int y = 0; y < align_patch_size; ++y) {
            uint8_t *it = ref_patch_with_border + (y + 1) * ref_step + 1;
            for (int x = 0; x < align_patch_size; ++x, ++it, ++it_dx, ++it_dy) {
                Vector3f J;
                J[0] = 0.5 * (it[1] - it[-1]);
                J[1] = 0.5 * (it[ref_step] - it[-ref_step]);
                J[2] = 1;
                *it_dx = J[0];
                *it_dy = J[1];
                H += J * J.transpose();
            }
        }
        Matrix3f Hinv = H.inverse();

        // H is singular, maybe caused by over explosure or completely dark parts
        if (isnan(Hinv(0, 0))) {
            cur_px_estimate << -1, -1;
            return false;
        }

        float mean_diff = 0;

        // Compute pixel location in new image:
        float u = cur_px_estimate.x();
        float v = cur_px_estimate.y();

        // termination condition
        const float min_update_squared = 0.03 * 0.03;
        const int cur_step = cur_img.step.p[0];
        Vector3f update;
        update.setZero();
        float chi2 = 0;
        for (int iter = 0; iter < n_iter; ++iter) {
            chi2 = 0;
            int u_r = floor(u);
            int v_r = floor(v);
            if (u_r < align_halfpatch_size || v_r < align_halfpatch_size ||
                u_r >= cur_img.cols - align_halfpatch_size ||
                v_r >= cur_img.rows - align_halfpatch_size)
                break;

            // compute interpolation weights
            float subpix_x = u - u_r;
            float subpix_y = v - v_r;
            float wTL = (1.0 - subpix_x) * (1.0 - subpix_y);
            float wTR = subpix_x * (1.0 - subpix_y);
            float wBL = (1.0 - subpix_x) * subpix_y;
            float wBR = subpix_x * subpix_y;

            // loop through search_patch, interpolate
            uint8_t *it_ref = ref_patch;
            float *it_ref_dx = ref_patch_dx;
            float *it_ref_dy = ref_patch_dy;
            Vector3f Jres;
            Jres.setZero();
            for (int y = 0; y < align_patch_size; ++y) {
                uint8_t *it = (uint8_t *) cur_img.data + (v_r + y - align_halfpatch_size) * cur_step + u_r -
                              align_halfpatch_size;
                for (int x = 0; x < align_patch_size; ++x, ++it, ++it_ref, ++it_ref_dx, ++it_ref_dy) {
                    float search_pixel = wTL * it[0] + wTR * it[1] + wBL * it[cur_step] + wBR * it[cur_step + 1];
                    float res = search_pixel - *it_ref + mean_diff;
                    Jres[0] -= res * (*it_ref_dx);
                    Jres[1] -= res * (*it_ref_dy);
                    Jres[2] -= res;
                    chi2 += res * res;
                }
            }
            update = Hinv * Jres;
            u += update[0];
            v += update[1];

            mean_diff += update[2];
            if (update[0] * update[0] + update[1] * update[1] < min_update_squared) {
                converged = true;
                break;
            }
        }

        if (isnan(u) || isnan(v)) {
            cur_px_estimate << -1, -1;
            return false;
        }
        cur_px_estimate << u, v;
        return converged;
    }

    bool Align1D(
            const cv::Mat &cur_img,
            const Eigen::Vector2f &dir, // direction in which the patch is allowed to move
            uint8_t *ref_patch_with_border,
            uint8_t *ref_patch,
            const int n_iter,
            Vector2f &cur_px_estimate,
            double &h_inv) {

        bool converged = false;

        // compute derivative of template and prepare inverse compositional
        float __attribute__ (( __aligned__ ( 16 ))) ref_patch_dv[align_patch_area];
        Matrix2f H;
        H.setZero();

        // compute gradient and hessian
        const int ref_step = align_patch_size + 2;
        float *it_dv = ref_patch_dv;
        for (int y = 0; y < align_patch_size; ++y) {
            uint8_t *it = ref_patch_with_border + (y + 1) * ref_step + 1;
            for (int x = 0; x < align_patch_size; ++x, ++it, ++it_dv) {
                Vector2f J;
                J[0] = 0.5 * (dir[0] * (it[1] - it[-1]) + dir[1] * (it[ref_step] - it[-ref_step]));
                J[1] = 1;
                *it_dv = J[0];
                H += J * J.transpose();
            }
        }
        h_inv = 1.0 / H(0, 0) * align_patch_size * align_patch_size;
        Matrix2f Hinv = H.inverse();
        float mean_diff = 0;

        // Compute pixel location in new image:
        float u = cur_px_estimate.x();
        float v = cur_px_estimate.y();

        // termination condition
        const float min_update_squared = 0.03 * 0.03;
        const int cur_step = cur_img.step.p[0];
        float chi2 = 0;
        Vector2f update;
        update.setZero();
        for (int iter = 0; iter < n_iter; ++iter) {
            int u_r = floor(u);
            int v_r = floor(v);
            if (u_r < align_halfpatch_size || v_r < align_halfpatch_size ||
                u_r >= cur_img.cols - align_halfpatch_size ||
                v_r >= cur_img.rows - align_halfpatch_size)
                break;

            if (isnan(u) ||
                isnan(v)) // TODO very rarely this can happen, maybe H is singular? should not be at corner.. check
                return false;

            // compute interpolation weights
            float subpix_x = u - u_r;
            float subpix_y = v - v_r;
            float wTL = (1.0 - subpix_x) * (1.0 - subpix_y);
            float wTR = subpix_x * (1.0 - subpix_y);
            float wBL = (1.0 - subpix_x) * subpix_y;
            float wBR = subpix_x * subpix_y;

            // loop through search_patch, interpolate
            uint8_t *it_ref = ref_patch;
            float *it_ref_dv = ref_patch_dv;
            float new_chi2 = 0.0;
            Vector2f Jres;
            Jres.setZero();
            for (int y = 0; y < align_patch_size; ++y) {
                uint8_t *it = (uint8_t *) cur_img.data + (v_r + y - align_halfpatch_size) * cur_step + u_r -
                              align_halfpatch_size;
                for (int x = 0; x < align_patch_size; ++x, ++it, ++it_ref, ++it_ref_dv) {
                    float search_pixel = wTL * it[0] + wTR * it[1] + wBL * it[cur_step] + wBR * it[cur_step + 1];
                    float res = search_pixel - *it_ref + mean_diff;
                    Jres[0] -= res * (*it_ref_dv);
                    Jres[1] -= res;
                    new_chi2 += res * res;
                }
            }

            if (iter > 0 && new_chi2 > chi2) {
                u -= update[0];
                v -= update[1];
                break;
            }

            chi2 = new_chi2;
            update = Hinv * Jres;
            u += update[0] * dir[0];
            v += update[0] * dir[1];
            mean_diff += update[1];


            if (update[0] * update[0] + update[1] * update[1] < min_update_squared) {
                converged = true;
                break;
            }
        }

        cur_px_estimate << u, v;
        return converged;
    }

}
