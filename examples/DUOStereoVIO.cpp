//
// Created by bobin on 17-10-9.
//


/**
 * DUO stereo visual-inertial odometry
*/

#include <opencv2/opencv.hpp>
#include <DUOLib.h>

#include "ygz/System.h"
#include "ygz/DUOReader.h"

using namespace std;
using namespace ygz;

#include "ygz/NumTypes.h"

void CALLBACK DUOCallback(const PDUOFrame pFrameData, void *pUserData) {
    PDUOFrame _pFrameData = pFrameData;
    System *system=(System *)pUserData;
    cv::Mat left, right;
    left.create(_pFrameData->height, _pFrameData->width, CV_8U);
    right.create(_pFrameData->height, _pFrameData->width, CV_8U);
    left.data = _pFrameData->leftData;
    right.data = _pFrameData->rightData;
    double timeStamp = pFrameData->timeStamp;
    ygz::VecIMU _imu_vec;
    if (pFrameData->IMUPresent) {
        for (int i = 0; i < pFrameData->IMUSamples; ++i) {
            float *acc_data = pFrameData->IMUData[i].accelData;
            float *gyro_data = pFrameData->IMUData[i].gyroData;
            ygz::IMUData _imu(acc_data[0], acc_data[1], acc_data[2], gyro_data[0], gyro_data[1], gyro_data[2], pFrameData->IMUData[i].timeStamp);
//            _imu.mfAcce = ygz::Vector3f(acc_data[0], acc_data[1], acc_data[2]);
//            _imu.mfGyro = ygz::Vector3f(gyro_data[0], gyro_data[1], gyro_data[2]);
            _imu_vec.push_back(_imu);
        }
    }
    system->AddStereoIMU(left, right, timeStamp, _imu_vec);
}

#define WIDTH    752
#define HEIGHT    480
#define FPS        30

//void DUO_init() {
//    DUOReader duo_reader;
//    duo_reader.OpenDUOCamera(WIDTH, HEIGHT, FPS);
//    duo_reader.SetGain(0);
////	duo_reader.SetExposure(50);
//    duo_reader.SetAutoExpose(true);
//    duo_reader.SetLed(25);
//    duo_reader.SetIMURate(200);
//    duo_reader.StartDUOFrame(DUOCallback);
//    duo_reader.CloseDUOCamera();
//}

int main(int argc, char **argv) {

    if (argc != 2) {
        LOG(INFO) << "Usage: EurocStereoVIO path_to_config" << endl;
        return 1;
    }

    FLAGS_logtostderr = true;
    google::InitGoogleLogging(argv[0]);

    string configFile(argv[1]);
    cv::FileStorage fsSettings(configFile, cv::FileStorage::READ);

    if (fsSettings.isOpened() == false) {
        LOG(FATAL) << "Cannot load the config file from " << argv[1] << endl;
    }

    System system(argv[1]);


    DUOReader duo_reader;
    duo_reader.OpenDUOCamera(WIDTH, HEIGHT, FPS);
    duo_reader.SetGain(10);
//	duo_reader.SetExposure(50);
    duo_reader.SetAutoExpose(true);
    duo_reader.SetLed(25);
    duo_reader.SetIMURate(200);
    duo_reader.SetUndistort(true);



    // read TBC
    cv::Mat Rbc, tbc;
    fsSettings["RBC"] >> Rbc;
    fsSettings["TBC"] >> tbc;
    if (!Rbc.empty() && tbc.empty()) {
        Matrix3d Rbc_;
        Vector3d tbc_;
        Rbc_ <<
             Rbc.at<double>(0, 0), Rbc.at<double>(0, 1), Rbc.at<double>(0, 2),
                Rbc.at<double>(1, 0), Rbc.at<double>(1, 1), Rbc.at<double>(1, 2),
                Rbc.at<double>(2, 0), Rbc.at<double>(2, 1), Rbc.at<double>(2, 2);
        tbc_ <<
             tbc.at<double>(0, 0), tbc.at<double>(1, 0), tbc.at<double>(2, 0);

        setting::TBC = SE3d(Rbc_, tbc_);
    }

    duo_reader.StartDUOFrame(DUOCallback, &system);
    duo_reader.CloseDUOCamera();



    return 0;
}

