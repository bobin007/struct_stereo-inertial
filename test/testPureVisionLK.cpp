#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "ygz/Frame.h"
#include "ygz/Feature.h"
#include "ygz/Settings.h"
#include "ygz/ORBExtractor.h"
#include "ygz/ORBMatcher.h"
#include "ygz/EurocReader.h"
#include "ygz/TrackerLK.h"
#include "ygz/BackendSlidingWindowG2O.h"
#include "ygz/Viewer.h"

using namespace std;
using namespace ygz;

/***
 * 本程序测试在EUROC数据集上双目纯视觉的Tracking
 */

// 路径
double discardtime = 0; // discard some frames in the beginning. for V101~7s

// string leftFolder = "/media/jp/JingpangPassport/3dataset/EuRoC-VIO/zipfiles/MH_01_easy/mav0/cam0/data";
// string rightFolder = "/media/jp/JingpangPassport/3dataset/EuRoC-VIO/zipfiles/MH_01_easy/mav0/cam1/data";
// string timeFolder = "/home/jp/opensourcecode/ygz-stereo-inertial/examples/EuRoC_TimeStamps/MH01.txt";
// string imuFolder = "/media/jp/JingpangPassport/3dataset/EuRoC-VIO/zipfiles/MH_01_easy/mav0/imu0/data.csv";
// string configFile = "/home/jp/opensourcecode/ygz-stereo-inertial/examples/EuRoC.yaml";
// string vocFile = "/home/jp/opensourcecode/ygz-stereo-inertial/Vocabulary/ORBvoc.bin";

// string leftFolder = "/var/dataset/euroc/MH_04_difficult/cam0/data";
// string rightFolder = "/var/dataset/euroc/MH_04_difficult/cam1/data";
// string imuFolder = "/var/dataset/euroc/MH_04_difficult/imu0/data.csv";
// string timeFolder = "/home/xiang/code/ygz-stereo-inertial/examples/EuRoC_TimeStamps/MH04.txt";
// string configFile = "/home/xiang/code/ygz-stereo-inertial/examples/EuRoC.yaml";
// string vocFile = "/home/xiang/code/ygz-stereo-inertial/Vocabulary/ORBvoc.bin";

string leftFolder = "/home/xiang/dataset/euroc/MH_01_easy/cam0/data";
string rightFolder = "/home/xiang/dataset/euroc/MH_01_easy/cam1/data";
string imuFolder = "/home/xiang/dataset/euroc/MH_01_easy/imu0/data.csv";
string timeFolder = "/home/xiang/code/ygz-stereo-inertial/examples/EuRoC_TimeStamps/MH01.txt";
string configFile = "/home/xiang/code/ygz-stereo-inertial/examples/EuRoC.yaml";
string vocFile = "/home/xiang/code/ygz-stereo-inertial/Vocabulary/ORBvoc.bin";

int start_index = 0; // 起始图像的索引

shared_ptr<TrackerLK> pTracker = nullptr;

int main(int argc, char **argv) {
    google::InitGoogleLogging(argv[0]);
    pTracker = shared_ptr<TrackerLK>(new TrackerLK);
    pTracker->TestTrackerLK();
    return 0;
}

void TrackerLK::TestTrackerLK() {

    vector<string> vstrImageLeft;
    vector<string> vstrImageRight;
    vector<double> vTimeStamp;
    VecIMU vimus;

    if ( LoadImages(leftFolder, rightFolder, timeFolder, vstrImageLeft, vstrImageRight, vTimeStamp) == false )
        return;
    if (LoadImus(imuFolder, vimus) == false )
        return;

    // Read rectification parameters
    cv::FileStorage fsSettings(configFile, cv::FileStorage::READ);
    if (!fsSettings.isOpened()) {
        cerr << "ERROR: Wrong path to settings" << endl;
        return;
    }

    cv::Mat K_l, K_r, P_l, P_r, R_l, R_r, D_l, D_r;
    fsSettings["LEFT.K"] >> K_l;
    fsSettings["RIGHT.K"] >> K_r;

    fsSettings["LEFT.P"] >> P_l;
    fsSettings["RIGHT.P"] >> P_r;

    fsSettings["LEFT.R"] >> R_l;
    fsSettings["RIGHT.R"] >> R_r;

    fsSettings["LEFT.D"] >> D_l;
    fsSettings["RIGHT.D"] >> D_r;

    int rows_l = fsSettings["LEFT.height"];
    int cols_l = fsSettings["LEFT.width"];
    int rows_r = fsSettings["RIGHT.height"];
    int cols_r = fsSettings["RIGHT.width"];

    if (K_l.empty() || K_r.empty() || P_l.empty() || P_r.empty() || R_l.empty() || R_r.empty() || D_l.empty() ||
        D_r.empty() ||
        rows_l == 0 || rows_r == 0 || cols_l == 0 || cols_r == 0) {
        cerr << "ERROR: Calibration parameters to rectify stereo are missing!" << endl;
        return;
    }

    cv::Mat M1l, M2l, M1r, M2r;
    cv::initUndistortRectifyMap(K_l, D_l, R_l, P_l.rowRange(0, 3).colRange(0, 3), cv::Size(cols_l, rows_l), CV_32F, M1l,
                                M2l);
    cv::initUndistortRectifyMap(K_r, D_r, R_r, P_r.rowRange(0, 3).colRange(0, 3), cv::Size(cols_r, rows_r), CV_32F, M1r,
                                M2r);

    const int nImages = vstrImageLeft.size();

    // Create camera object
    setting::initSettings();
    float fx = fsSettings["Camera.fx"];
    float fy = fsSettings["Camera.fy"];
    float cx = fsSettings["Camera.cx"];
    float cy = fsSettings["Camera.cy"];
    float bf = fsSettings["Camera.bf"];

    shared_ptr<CameraParam> camera(new CameraParam(fx, fy, cx, cy, bf));
    this->mpCam = camera;

    srand(time(nullptr));

    // create a backend
    shared_ptr<BackendSlidingWindowG2O> backend(new BackendSlidingWindowG2O(pTracker));
    mpBackEnd = backend;

    // and other things
    mpExtractor = shared_ptr<ORBExtractor>(new ORBExtractor(ORBExtractor::FAST_SINGLE_LEVEL));
    mpMatcher = shared_ptr<ORBMatcher>(new ORBMatcher);

    // Main loop
    cv::Mat imLeft, imRight, imLeftRect, imRightRect;
    size_t imuIndex = 0;

    // set up visualization
    shared_ptr<Viewer> pviewer(new Viewer);
    mpViewer = pviewer;
    mbVisionOnlyMode = true;
    double tstart = vTimeStamp[0];
    for (int ni = start_index; ni < nImages; ni++) {

        // Read left and right images from file
        imLeft = cv::imread(vstrImageLeft[ni], CV_LOAD_IMAGE_UNCHANGED);
        imRight = cv::imread(vstrImageRight[ni], CV_LOAD_IMAGE_UNCHANGED);

        cv::remap(imLeft, imLeftRect, M1l, M2l, cv::INTER_LINEAR);
        cv::remap(imRight, imRightRect, M1r, M2r, cv::INTER_LINEAR);

        VecIMU vimu;

        double tframe = vTimeStamp[ni];

        if (tframe - tstart < discardtime)
            continue;

        while (1) {
            const ygz::IMUData &imudata = vimus[imuIndex];
            if (imudata.mfTimeStamp >= tframe)
                break;
            vimu.push_back(imudata);
            imuIndex++;
        }

        std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
        SE3d Twb = this->InsertStereo(imLeftRect, imRightRect, tframe, vimu);
        LOG(INFO) << "current Twb = \n" << Twb.matrix() << endl;
        std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
        double timeCost = std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();
        LOG(INFO) << "Tracker cost time: " << timeCost << endl;
    }

    mpBackEnd->Shutdown();
    sleep(2000);

    setting::destroySettings();
}
